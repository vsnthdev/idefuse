﻿using System;
using System.Collections.Generic;
using System.IO;

namespace IDEFuse.DirectoryManager
{
    public class DirectoryManager
    {
        //
        // [INFO]: This class check if the program is running for the first time, if yes then it creates the required directories
        //

        #region Custom Properties

        // The actual AppName string
        public string AppName { get; set; }

        // Will be true if this is the first time user launched the application
        public bool FirstLaunch { get; private set; } = false;

        // The MyDocuments/AppName folder
        public string PersonalDirectory { get; private set; }

        // The MyDocuments/AppName/Preferences folder
        public string SettingsDirectory { get; private set; }

        // The MyDocuments/AppName/Logs folder
        public string LogDirectory { get; private set; }

        // The AppData folder for the user, where we can store some encrypted things.
        public string AppDataDirectory { get; private set; }

        #endregion

        #region Custom Methods

        public void Initialize()
        {
            PersonalDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), AppName);
            SettingsDirectory = Path.Combine(PersonalDirectory, "Preferences");
            LogDirectory = Path.Combine(PersonalDirectory, "Logs");
            AppDataDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), AppName);

            if (Directory.Exists(SettingsDirectory) != true)
            {
                FirstLaunch = true;
                Directory.CreateDirectory(PersonalDirectory);
                Directory.CreateDirectory(SettingsDirectory);
                Directory.CreateDirectory(LogDirectory);
                Directory.CreateDirectory(AppDataDirectory);
            } else
            {
                FirstLaunch = false;
            }
        }

        #endregion
    }
}
