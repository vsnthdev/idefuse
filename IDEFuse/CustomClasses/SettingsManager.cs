﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace IDEFuse.Settings
{
    public class SettingsManager
    {

        //
        // [INFO]: This class encrypts the preferences.config file and loads it back
        //

        #region Custom Properties

        // The AES_KEY for encrypting and decrypting the preferences
        // [Note]: Make sure that we use different AES_KEYS for different Fuse Apps
        public string ENC_KEY { get; set; }

        // The variable where the path to preferences file is stored
        public string PreferencesFilePath { get; set; }

        #endregion

        #region Custom Variables

        // The AES Object which will do the encryption and decryption
        private RijndaelManaged AESObj = new RijndaelManaged();

        // Holds the byte array for AES_KEY
        private byte[] passBytes;

        // Holds the byte array for AES_KEY after padding
        private byte[] EncryptionkeyBytes;

        // Holds the bytes length of AES_KEY
        private int len;

        #endregion

        #region Custom Methods

        // This method would set the properties and values to some variables and setup the AESObject for work and returns nothing
        public void InitializeManager()
        {
            // Set the cypher mode
            AESObj.Mode = CipherMode.CBC;

            // Set the encryption padding
            AESObj.Padding = PaddingMode.PKCS7;

            // Specify a key size and a block size for encrypted file
            AESObj.KeySize = 0x80;
            AESObj.BlockSize = 0x80;

            // Convert the AES_KEY into a byte array
            passBytes = Encoding.UTF8.GetBytes(ENC_KEY);

            // Create a new variable for AES_KEY
            EncryptionkeyBytes = new byte[0x10];

            // Holds the bytes length of AES_KEY
            len = passBytes.Length;


            if (len > EncryptionkeyBytes.Length)
            {
                len = EncryptionkeyBytes.Length;
            }
            Array.Copy(passBytes, EncryptionkeyBytes, len);

            // Set the KEY and IV to AES object
            AESObj.Key = EncryptionkeyBytes;
            AESObj.IV = EncryptionkeyBytes;
        }

        // This method would decrypt an encrypted string and return the decrypted string
        public string _d(string EncryptedString)
        {
            // Make the encryptedTextByte variable accessible outside the try catch
            byte[] encryptedTextByte;

            try
            {
                encryptedTextByte = Convert.FromBase64String(EncryptedString);
            } catch
            {
                // Throw the exception
                throw new Exception("Could not load the preferences. Failed to decrypt.");
            }

            // Try to decrypt the encrypted string
            try
            {
                byte[] TextByte = AESObj.CreateDecryptor().TransformFinalBlock(encryptedTextByte, 0, encryptedTextByte.Length);
                return Encoding.UTF8.GetString(TextByte).ToString();
            } catch
            {
                // Throw the exception
                throw new Exception("Could not load the preferences. Failed to decrypt.");
            }
        }

        // This method would encrypt a given string and return the encrypted string
        public string _e(string PlainString)
        {
            // Convert the AES_KEY into a byte array
            byte[] EncryptionkeyBytes = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

            if (len > EncryptionkeyBytes.Length)
            {
                len = EncryptionkeyBytes.Length;
            }
            Array.Copy(passBytes, EncryptionkeyBytes, len);

            // Create a crypto transform object and connect it with the AES object
            ICryptoTransform objtransform = AESObj.CreateEncryptor();

            // Encrypt the JSON string and convert into byte array
            byte[] textDataByte = Encoding.UTF8.GetBytes(PlainString);

            // Return the encrypted string
            return Convert.ToBase64String(objtransform.TransformFinalBlock(textDataByte, 0, textDataByte.Length));
        }

        #endregion
    }
}
