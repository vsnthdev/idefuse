﻿using System;
using System.Reflection;

namespace IDEFuse.Settings
{
    public class appSettings
    {
        //
        // [INFO]: This class is operated by SettingsManager class. It exports all the preferences into preferences.config and
        // Allows the program to modify certain values on runtime
        //

        #region Public Preferences

        // Settings that would be saved into the configuration file
        public virtual int Theme { get; set; } = 0;

        #endregion

        #region Private Preferences

        // Settings that will not get saved into configuration file
        private string AppEdition = "Professional";

        #endregion

        #region Custom Methods

        // This function will return the version number in the following format: Major.Minior [Example: 19.01]
        public string GetAppVersion()
        {
            Version AppVersion = Assembly.GetCallingAssembly().GetName().Version;

            if (AppVersion.Minor.ToString().Length == 1)
            {
                return AppVersion.Major.ToString() + ".0" + AppVersion.Minor.ToString();
            }
            else
            {
                return AppVersion.Major.ToString() + "." + AppVersion.Minor.ToString();
            }
        }

        #endregion
    }
}
