﻿using System;
using System.IO;

namespace IDEFuse.LogManager
{
    public class LogManager
    {
        //
        // [INFO]: This class manages the log files for Fuse Apps
        //

        #region Custom Properties

        // The directory where logs are stored
        private string LogPath { get; set; }

        // The path of stranded error log file
        private string ErrorLogFile;

        // The path of stranded output log file
        private string InfoLogFile;

        #endregion

        #region Initialization

        public LogManager(string LogPath, string AppName)
        {
            this.LogPath = LogPath;

            ErrorLogFile = Path.Combine(LogPath, "stderr.log");
            InfoLogFile = Path.Combine(LogPath, "stdout.log");

            LogInfo("Application Started.");
        }

        #endregion

        #region Custom Methods

        public void LogInfo(string Message)
        {
            File.AppendAllText(InfoLogFile, "[" + DateTime.Now.ToString() + "]: " + Message + "\n");
        }

        public void LogError(string Message)
        {
            File.AppendAllText(ErrorLogFile, "[" + DateTime.Now.ToString() + "]: " + Message + "\n");
        }

        #endregion
    }
}
