﻿namespace IDEFuse.CustomComponents
{
    partial class StartPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartPage));
            this.pnlSidebar = new System.Windows.Forms.Panel();
            this.lblAppVersion = new System.Windows.Forms.Label();
            this.btnOpenDocument = new System.Windows.Forms.LinkLabel();
            this.btnCreateNewDocument = new System.Windows.Forms.LinkLabel();
            this.lblStartPage = new System.Windows.Forms.Label();
            this.pnlBranding = new System.Windows.Forms.Panel();
            this.lblAppDescription = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblAppName = new System.Windows.Forms.Label();
            this.picAppIcon = new System.Windows.Forms.PictureBox();
            this.pnlSidebar.SuspendLayout();
            this.pnlBranding.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAppIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSidebar
            // 
            this.pnlSidebar.BackColor = System.Drawing.SystemColors.Control;
            this.pnlSidebar.Controls.Add(this.lblAppVersion);
            this.pnlSidebar.Controls.Add(this.btnOpenDocument);
            this.pnlSidebar.Controls.Add(this.btnCreateNewDocument);
            this.pnlSidebar.Controls.Add(this.lblStartPage);
            this.pnlSidebar.Controls.Add(this.pnlBranding);
            this.pnlSidebar.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlSidebar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlSidebar.Location = new System.Drawing.Point(0, 0);
            this.pnlSidebar.Name = "pnlSidebar";
            this.pnlSidebar.Size = new System.Drawing.Size(294, 630);
            this.pnlSidebar.TabIndex = 0;
            // 
            // lblAppVersion
            // 
            this.lblAppVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblAppVersion.AutoSize = true;
            this.lblAppVersion.Location = new System.Drawing.Point(12, 608);
            this.lblAppVersion.Name = "lblAppVersion";
            this.lblAppVersion.Size = new System.Drawing.Size(42, 13);
            this.lblAppVersion.TabIndex = 1;
            this.lblAppVersion.Text = "Version";
            // 
            // btnOpenDocument
            // 
            this.btnOpenDocument.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(136)))), ((int)(((byte)(209)))));
            this.btnOpenDocument.AutoSize = true;
            this.btnOpenDocument.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.btnOpenDocument.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            this.btnOpenDocument.Location = new System.Drawing.Point(35, 208);
            this.btnOpenDocument.Name = "btnOpenDocument";
            this.btnOpenDocument.Size = new System.Drawing.Size(83, 13);
            this.btnOpenDocument.TabIndex = 1;
            this.btnOpenDocument.TabStop = true;
            this.btnOpenDocument.Text = "Open document";
            this.btnOpenDocument.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            // 
            // btnCreateNewDocument
            // 
            this.btnCreateNewDocument.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(136)))), ((int)(((byte)(209)))));
            this.btnCreateNewDocument.AutoSize = true;
            this.btnCreateNewDocument.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.btnCreateNewDocument.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            this.btnCreateNewDocument.Location = new System.Drawing.Point(35, 184);
            this.btnCreateNewDocument.Name = "btnCreateNewDocument";
            this.btnCreateNewDocument.Size = new System.Drawing.Size(111, 13);
            this.btnCreateNewDocument.TabIndex = 1;
            this.btnCreateNewDocument.TabStop = true;
            this.btnCreateNewDocument.Text = "Create new document";
            this.btnCreateNewDocument.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(169)))), ((int)(((byte)(244)))));
            // 
            // lblStartPage
            // 
            this.lblStartPage.AutoSize = true;
            this.lblStartPage.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold);
            this.lblStartPage.Location = new System.Drawing.Point(19, 152);
            this.lblStartPage.Name = "lblStartPage";
            this.lblStartPage.Size = new System.Drawing.Size(99, 25);
            this.lblStartPage.TabIndex = 1;
            this.lblStartPage.Text = "Start Page";
            // 
            // pnlBranding
            // 
            this.pnlBranding.Controls.Add(this.lblAppDescription);
            this.pnlBranding.Controls.Add(this.panel1);
            this.pnlBranding.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBranding.Location = new System.Drawing.Point(0, 0);
            this.pnlBranding.Name = "pnlBranding";
            this.pnlBranding.Size = new System.Drawing.Size(294, 129);
            this.pnlBranding.TabIndex = 1;
            // 
            // lblAppDescription
            // 
            this.lblAppDescription.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblAppDescription.Location = new System.Drawing.Point(10, 92);
            this.lblAppDescription.Name = "lblAppDescription";
            this.lblAppDescription.Size = new System.Drawing.Size(275, 20);
            this.lblAppDescription.TabIndex = 1;
            this.lblAppDescription.Text = "Application Tagline";
            this.lblAppDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblAppName);
            this.panel1.Controls.Add(this.picAppIcon);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(294, 103);
            this.panel1.TabIndex = 3;
            // 
            // lblAppName
            // 
            this.lblAppName.Font = new System.Drawing.Font("Segoe UI Semilight", 20F);
            this.lblAppName.Location = new System.Drawing.Point(85, 32);
            this.lblAppName.Name = "lblAppName";
            this.lblAppName.Size = new System.Drawing.Size(188, 39);
            this.lblAppName.TabIndex = 2;
            this.lblAppName.Text = "IDE Fuse";
            // 
            // picAppIcon
            // 
            this.picAppIcon.Location = new System.Drawing.Point(22, 23);
            this.picAppIcon.Name = "picAppIcon";
            this.picAppIcon.Size = new System.Drawing.Size(57, 56);
            this.picAppIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picAppIcon.TabIndex = 1;
            this.picAppIcon.TabStop = false;
            // 
            // StartPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 630);
            this.Controls.Add(this.pnlSidebar);
            this.DockAreas = WeifenLuo.WinFormsUI.Docking.DockAreas.Document;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StartPage";
            this.TabText = "Start Page";
            this.Tag = "start_page";
            this.Text = "Start Page";
            this.Load += new System.EventHandler(this.StartPage_Load);
            this.pnlSidebar.ResumeLayout(false);
            this.pnlSidebar.PerformLayout();
            this.pnlBranding.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picAppIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSidebar;
        private System.Windows.Forms.Panel pnlBranding;
        public System.Windows.Forms.PictureBox picAppIcon;
        public System.Windows.Forms.Label lblAppName;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Label lblAppDescription;
        private System.Windows.Forms.Label lblStartPage;
        public System.Windows.Forms.LinkLabel btnCreateNewDocument;
        public System.Windows.Forms.LinkLabel btnOpenDocument;
        public System.Windows.Forms.Label lblAppVersion;
    }
}
