﻿using Gecko;
using WeifenLuo.WinFormsUI.Docking;

namespace IDEFuse.CustomComponents
{
    public partial class GeckoBrowser : DockContent
    {
        #region Initialization

        public GeckoBrowser()
        {
            InitializeComponent();

            // Initialize the GeckoFX Engine
            Xpcom.Initialize("Firefox");

            // Set the user agent
            GeckoPreferences.User["general.useragent.override"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0";
        }

        #endregion

        #region Custom Methods

        public void RenderHTML(string RAW_HTML)
        {
            Engine.LoadHtml(RAW_HTML);
        }

        #endregion
    }
}
