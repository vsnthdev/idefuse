﻿using IDEFuse.CustomClasses;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace IDEFuse.CustomComponents
{
    public partial class StartPage : DockContent
    {
        public int ComponentTheme;

        public StartPage()
        {
            InitializeComponent();

            // Get the Application Name and set it
            lblAppName.Text = Application.ProductName.ToString();

            // Get the Application Tag line and set it
            AssemblyInfo assemblyInfo = new AssemblyInfo(Assembly.GetCallingAssembly());
            lblAppDescription.Text = assemblyInfo.Description.ToString();
        }

        private void StartPage_Load(object sender, System.EventArgs e)
        {
            // Switch the theme according to the inherited class's set values
            switch (ComponentTheme)
            {
                case 0:
                    BackColor = Color.FromArgb(238, 238, 242);
                    pnlSidebar.BackColor = Color.FromArgb(228, 228, 232);
                    pnlBranding.BackColor = Color.FromArgb(220, 220, 224);
                    lblAppName.ForeColor = Color.FromArgb(118, 118, 122);
                    lblAppDescription.ForeColor = Color.FromArgb(150, 150, 154);
                    lblStartPage.ForeColor = Color.FromArgb(100, 100, 103);
                    lblAppVersion.ForeColor = Color.FromArgb(180, 180, 184);
                    break;

                case 1:
                    BackColor = Color.FromArgb(45, 45, 48);
                    pnlSidebar.BackColor = Color.FromArgb(40, 40, 43);
                    pnlBranding.BackColor = Color.FromArgb(30, 30, 33);
                    lblAppName.ForeColor = Color.FromArgb(180, 180, 183);
                    lblAppDescription.ForeColor = Color.FromArgb(130, 130, 133);
                    lblStartPage.ForeColor = Color.FromArgb(200, 200, 203);
                    lblAppVersion.ForeColor = Color.FromArgb(100, 100, 103);
                    break;
            }
        }
    }
}
