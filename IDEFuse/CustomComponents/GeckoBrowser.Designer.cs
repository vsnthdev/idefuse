﻿namespace IDEFuse.CustomComponents
{
    partial class GeckoBrowser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Engine = new Gecko.GeckoWebBrowser();
            this.SuspendLayout();
            // 
            // Engine
            // 
            this.Engine.ConsoleMessageEventReceivesConsoleLogCalls = true;
            this.Engine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Engine.FrameEventsPropagateToMainWindow = false;
            this.Engine.Location = new System.Drawing.Point(0, 0);
            this.Engine.Name = "Engine";
            this.Engine.Size = new System.Drawing.Size(974, 636);
            this.Engine.TabIndex = 0;
            this.Engine.UseHttpActivityObserver = false;
            // 
            // GeckoBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(974, 636);
            this.Controls.Add(this.Engine);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "GeckoBrowser";
            this.ShowHint = WeifenLuo.WinFormsUI.Docking.DockState.Document;
            this.TabText = "Gecko Browser";
            this.Text = "Gecko Browser";
            this.ResumeLayout(false);

        }

        #endregion

        public Gecko.GeckoWebBrowser Engine;
    }
}
