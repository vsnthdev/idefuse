﻿using ScintillaNET;
using System.Drawing;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace IDEFuse.CustomComponents
{
    public partial class CodeEditor : DockContent
    {
        public int ComponentTheme = 1;

        #region Initialization

        public CodeEditor()
        {
            InitializeComponent();

            // [TEMP] Remove the horizontal scrollbar
            Editor.HScrollBar = false;
        }

        private void CodeEditor_Load(object sender, System.EventArgs e)
        {
            // Initialize the Basic Colors and fonts for Scintilla
            initColors(ComponentTheme);
        
            // Initialize the line margin
            initMargins(ComponentTheme);

            // Setup the bookmark margin
            initBookmarkMargin(ComponentTheme);
        }

        #endregion

        #region Scintilla Configuration

        private void initColors(int ScintillaTheme)
        {
            switch (ScintillaTheme)
            {
                case 0:
                    // Set the text selection color
                    Editor.SetSelectionBackColor(true, System.Drawing.Color.FromArgb(230, 230, 230));

                    // Reset any custom applied styles to default
                    Editor.StyleResetDefault();

                    // Set the font family
                    Editor.Styles[Style.Default].Font = "Consolas";

                    // Set the font size
                    Editor.Styles[Style.Default].Size = 16;

                    // Set the background color
                    Editor.Styles[Style.Default].BackColor = Color.White;

                    // Set the text color and caret color
                    Editor.Styles[Style.Default].ForeColor = Color.FromArgb(100, 100, 100);
                    Editor.CaretForeColor = Color.FromArgb(100, 100, 100);

                    // Set the caret width
                    Editor.CaretWidth = 2;

                    // Apply the styles
                    Editor.StyleClearAll();

                    break;
                case 1:
                    // Set the text selection color
                    Editor.SetSelectionBackColor(true, System.Drawing.Color.FromArgb(65, 65, 68));

                    // Reset any custom applied styles to default
                    Editor.StyleResetDefault();

                    // Set the font family
                    Editor.Styles[Style.Default].Font = "Consolas";

                    // Set the font size
                    Editor.Styles[Style.Default].Size = 16;

                    // Set the background color
                    Editor.Styles[Style.Default].BackColor = Color.FromArgb(35, 35, 38);

                    // Set the text color and caret color
                    Editor.Styles[Style.Default].ForeColor = Color.Gainsboro;
                    Editor.CaretForeColor = Color.Gainsboro;

                    // Set the caret width
                    Editor.CaretWidth = 2;

                    // Apply the styles
                    Editor.StyleClearAll();

                    break;
            }
        }

        private void initMargins(int ScintillaTheme)
        {
            // Get the second margin bar
            var nums = Editor.Margins[1];

            // Set a width of 30 to the margin
            nums.Width = 30;

            // Set the margin type to number, for showing line numbers
            nums.Type = MarginType.Number;

            // Set the numbers margin to click sensitive
            nums.Sensitive = true;

            // Set the mask as zero for the numbers margin
            nums.Mask = 0;

            // Set the indent guide lines color
            Editor.Styles[Style.IndentGuide].ForeColor = Color.Silver;

            // Finally Link the margin click event
            Editor.MarginClick += Editor_MarginClick;

            switch (ScintillaTheme)
            {
                case 0:
                    // Set the margin background color
                    Editor.Styles[Style.LineNumber].BackColor = Color.WhiteSmoke;

                    // Set the line number font color
                    Editor.Styles[Style.LineNumber].ForeColor = Color.Silver;

                    // Set the indent guide background color
                    Editor.Styles[Style.IndentGuide].BackColor = Color.White;

                    break;
                case 1:
                    // Set the margin background color
                    Editor.Styles[Style.LineNumber].BackColor = Color.FromArgb(55, 55, 58);

                    // Set the line number font color
                    Editor.Styles[Style.LineNumber].ForeColor = Color.DarkGray;

                    // Set the indent guide background color
                    Editor.Styles[Style.IndentGuide].BackColor = Color.FromArgb(35, 35, 38);

                    break;
            }
        }

        private void initBookmarkMargin(int ScintillaTheme)
        {
            // Get the third margin bar
            var margin = Editor.Margins[2];

            // Set the width as 20
            margin.Width = 20;

            // Make it click sensitive
            margin.Sensitive = true;

            // Set the margin type as a symbol
            margin.Type = MarginType.Symbol;

            // Set the mask
            margin.Mask = (1 << 2);

            // Set the bookmark symbol
            margin.Cursor = MarginCursor.Arrow;

            // Get the relative marker for bookmark margin
            var marker = Editor.Markers[2];

            // Set the symbol as bookmark
            marker.Symbol = MarkerSymbol.Bookmark;

            // Set the background color of the marker
            marker.SetBackColor(Color.FromArgb(255, 255, 87, 34));

            switch (ScintillaTheme)
            {
                case 0:
                    // Set the font color as black
                    marker.SetForeColor(Color.White);

                    break;
                case 1:
                    // Set the font color as black
                    marker.SetForeColor(Color.Black);

                    break;
            }

            // Set transparency
            marker.SetAlpha(100);
        }

        #endregion

        #region Control Events

        private void Editor_MarginClick(object sender, MarginClickEventArgs e)
        {
            // Check if the click event fired by clicking the bookmark margin
            if (e.Margin == 2)
            {
                // Variable with the mask as uint and const
                const uint mask = (1 << 2);

                // Get the current line
                var line = Editor.Lines[Editor.LineFromPosition(e.Position)];

                // Check if the current line's mask greater than zero
                if ((line.MarkerGet() & mask) > 0)
                {
                    // Remove existing bookmark
                    line.MarkerDelete(2);
                } else
                {
                    // Add bookmark
                    line.MarkerAdd(2);
                }
            }
        }

        #endregion
    }
}
